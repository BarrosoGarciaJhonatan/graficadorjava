/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package graficas;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author JHONATAN
 */
public class About extends JFrame{
    private Graficador graficador;
    
    public About(Graficador g){   
        super("Acerca");
        this.graficador = g;
        init();
        pack ();        
        ImageIcon icono = new ImageIcon(getClass().getResource("images/calculator.png")); 
        this.setIconImage(icono.getImage());
        this.setDefaultCloseOperation (JFrame.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setResizable(false);
    }
    private void init(){
        Container content = getContentPane (); 
        ImageIcon logo=new ImageIcon(graficador.getIto().getImage().getScaledInstance(200, 200, Image.SCALE_SMOOTH));
        JLabel titulo = new JLabel(logo);
        titulo.setBackground(Color.WHITE);
        titulo.setOpaque(true);
        titulo.setPreferredSize(new Dimension(300,200));
        content.add(titulo,BorderLayout.CENTER);
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        JLabel enc = new JLabel("Graficador de funciones matemáticas.");
        enc.setPreferredSize(new Dimension(300,40));
        enc.setFont(new Font("Arial",Font.BOLD,17));
        enc.setBackground(Color.WHITE);
        enc.setOpaque(true);
        // Descripción
        JPanel des = new JPanel();
        des.setLayout(new GridLayout(3,1));
        Font fuente = new Font("Arial",Font.BOLD,13);
        Font info = new Font("Arial",Font.PLAIN,13);
        JLabel vj = new JLabel("Java: ");
        vj.setFont(fuente);
        JLabel version = new JLabel("Versión de la aplicación: ");
        version.setFont(fuente);
        
        JPanel p1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        p1.add(vj);
        JLabel rV = new JLabel("11.0.8; OpenJDK 64-Bit Server VM 11.0.8+10");
        rV.setFont(info);
        p1.add(rV);
        JPanel p2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        p2.add(version);
        JLabel vA = new JLabel("1.0.0");
        vA.setFont(info);
        p2.add(vA);
        des.add(p2);
        des.add(p1);
        p1.setBackground(Color.WHITE);
        p2.setBackground(Color.WHITE);
        des.setBackground(Color.WHITE);
        JLabel autor = new JLabel("@Jhonatan Barroso García");
        autor.setFont(fuente);
        autor.setBackground(Color.WHITE);
        autor.setOpaque(true);
        autor.setHorizontalAlignment(JLabel.RIGHT);
        autor.setForeground(Color.BLUE);
        panel.add(enc, BorderLayout.NORTH);
        panel.add(des,BorderLayout.CENTER);
        panel.add(autor, BorderLayout.SOUTH);
        content.add(panel,BorderLayout.SOUTH);
    }
    
}
