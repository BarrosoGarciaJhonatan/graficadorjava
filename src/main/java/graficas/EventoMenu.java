/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package graficas;

import java.awt.FileDialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;


/**
 *
 * @author JHONATAN
 */
 public class EventoMenu implements ActionListener,KeyListener{
     
     private Graficador ventana;
     
     public EventoMenu(Graficador ventana){
         this.ventana = ventana;
     }

        @Override
        public void actionPerformed(ActionEvent evt) {
            Object source = evt.getSource ();
            
            if(source == ventana.getCatalogo()){
                
                ventana.getfFrame().setVisible(true);  
                
            }else if(source == ventana.getGraficas()){
                
                ventana.getFunciones().llenar();
                ventana.getFunciones().setVisible(true);
                
            }if(source == ventana.getEcuacion()){
                
                ventana.getContentPane().add("North", ventana.getDisplayPanel2()); 
                ventana.getDisplayPanel2().updateUI();
                
            }else if(source == ventana.getBtnLimpiar()){
                
                ventana.remove(ventana.getDisplayPanel2());
                ventana.getDisplayPanel1().updateUI();
                
            }else if(source == ventana.getSalir()){
                
                System.exit(0);
                
            }else if(source == ventana.getGuardar()){
                
                FileDialog chooser = new FileDialog(ventana, "Use una extensión .png o .jpg ", FileDialog.SAVE);
                chooser.setVisible(true);
                String filename = chooser.getFile();
                if (filename != null) {
                    ventana.guardar(chooser.getDirectory() + File.separator + chooser.getFile());
                }
                
            }else if(source == ventana.getAcerca()){
                ventana.getfAbout().setVisible(true);
            }

        }

    @Override
    public void keyTyped(KeyEvent evt) {}

    @Override
    public void keyPressed(KeyEvent evt) {
        //JOptionPane.showMessageDialog(null,"Evento del teclado");
        int code = evt.getKeyCode();
        
        if(code == KeyEvent.VK_A)
           ventana.getfAbout().setVisible(true);
        
        else if(code == KeyEvent.VK_F){
            
                ventana.getFunciones().llenar();
                ventana.getFunciones().setVisible(true); 
        }
  
    }

    @Override
    public void keyReleased(KeyEvent evt) {}
     
 }