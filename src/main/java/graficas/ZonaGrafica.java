/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package graficas;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Line2D;
import javax.swing.JPanel;
import org.nfunk.jep.type.Complex;

/**
 *
 * @author JHONATAN
 */
public class ZonaGrafica extends JPanel implements MouseListener, MouseMotionListener 
 {
    private int     offsetX, offsetY;   
    private boolean dragging; 
    private Graficador contenedor;
    ZonaGrafica(Graficador contenedor) {
        this.contenedor = contenedor;
        setBackground(Color.white);             
        offsetX=contenedor.getX0(); offsetY=contenedor.getY0();        
        addMouseListener(this);
        addMouseMotionListener(this);
    }
    
    
    @Override
     public void mousePressed(MouseEvent evt) 
     {  
                                     
         if (dragging)  
            return;
         int x = evt.getX();  // clic inicial
         int y = evt.getY();
         offsetX = x - contenedor.getX0();  
         offsetY = y - contenedor.getY0();
         dragging = true;                     
      }


    @Override
      public void mouseReleased(MouseEvent evt) 
      {                         
          dragging = false;        
          repaint();          
      }


   @Override
   public void mouseDragged(MouseEvent evt){   
          int x0;
          int y0;
          if (dragging == false)  
            return;

          int x = evt.getX();   // posición del mouse
          int y = evt.getY();
          x0 = x - offsetX;     // mover origen
          y0 = y - offsetY;    
          contenedor.setX0(x0);
          contenedor.setY0(y0);
          repaint();
      }

  //el resto hace nada 
  @Override
  public void mouseMoved(MouseEvent evt) {}           
  @Override
  public void mouseClicked(MouseEvent evt) { }
  @Override
  public void mouseEntered(MouseEvent evt) {}
  @Override
  public void mouseExited(MouseEvent evt) { }            

 
  @Override
  public void paintComponent(Graphics g) 
  {                   
       
        super.paintComponent(g);       // clear to background color  
                           	 
        Graficar(g, contenedor.getX0(), contenedor.getY0());                      
                        
       
 }
 
 public void Graficar(Graphics ap, int xg, int yg){

  int xi=0,yi=0,xi1=0,yi1=0,numPuntos=1;
  int cxmin,cxmax,cymin,cymax;
  double valxi=0.0, valxi1=0.0, valyi=0.0,valyi1=0.0; 

  //convertimos el objeto ap en un objeto Graphics2D para usar los métodos Java2D
  Graphics2D g = (Graphics2D) ap;
  //g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON); 
  /** Antialiasing */
        RenderingHints rh =
            new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            rh.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            g.setRenderingHints(rh);        

  g.setFont(contenedor.FT12); 
  g.setPaint(new Color(0,0,150)); 

  //eje Y
  g.draw(new Line2D.Double(xg, 10, xg,contenedor.getGalto()-10)); 
  //eje X
  g.draw(new Line2D.Double(10, yg, contenedor.getGancho()-10, yg));

  contenedor.setXmin(-1.0*xg/contenedor.getEscalaX());
  contenedor.setXmax(1.0*(contenedor.getGancho()-xg)/contenedor.getEscalaX());
  cxmin=(int)Math.round(contenedor.getXmin()); //pantalla
  cxmax=(int)Math.round(contenedor.getXmax());
  cymin=(int)Math.round(1.0*(yg-contenedor.getGalto())/contenedor.getEscalaY());
  cymax=(int)Math.round(1.0*yg/contenedor.getEscalaY());

  numPuntos=contenedor.getGancho(); //num pixels

  g.setPaint(Color.gray); 
  g.setFont(contenedor.FT12);

  //marcas en los ejes (ticks)
  if(contenedor.getEscalaX()>5)
  {
    for(int i=cxmin+1;i<cxmax;i++)
    {  g.draw(new Line2D.Double(xg+i*contenedor.getEscalaX(), yg-2, xg+i*contenedor.getEscalaX() , yg+2));
       if(i>0)
          g.drawString(""+i, xg+i*contenedor.getEscalaX()-2, yg+12);
       if(i<0)
         g.drawString(""+i, xg+i*contenedor.getEscalaX()-6, yg+12); 
    }
  }

  if(contenedor.getEscalaY()>5)
  { 
    for(int i=cymin+1;i<cymax;i++)
    {  g.draw(new Line2D.Double(xg-2, yg-i*contenedor.getEscalaY(), xg+2 , yg-i*contenedor.getEscalaY()));  
       if(i>0)
          g.drawString(""+i, xg-12,yg-i*contenedor.getEscalaY()+3 );
       if(i<0)
         g.drawString(""+i, xg-14,yg-i*contenedor.getEscalaY()+3 );
   }
  }
  g.setPaint(new Color(50,100,0));

  g.setStroke(contenedor.GROSOR1);
  
  int c = contenedor.getCont();
  if(!contenedor.getTffun().getText().equals("")){
      contenedor.getMiEvaluador().parseExpression(contenedor.getTffun().getText());
      contenedor.setErrorEnExpresion(contenedor.getMiEvaluador().hasError());//hay error? 
      if(!contenedor.isErrorEnExpresion()){
         if(c>=contenedor.getExp().length){
             c = 0;
             contenedor.setCont(0);
         }
        contenedor.getExp()[c]=""+contenedor.getTffun().getText();
        contenedor.setCont(c+1);
        contenedor.getTffun().setText("");
        contenedor.setSeGrafico(true);
      }else{
          contenedor.setSeGrafico(false);
          contenedor.getMensaje().setText("Hay un error en la sintaxis de la función.");
          contenedor.getMensaje().setForeground(Color.red);
          contenedor.getTffun().setForeground(Color.red);
          contenedor.getIconoE().setIcon(contenedor.getImagenX());
      }
  }else{
      contenedor.getMensaje().setText("Debe ingresar la función.");
      contenedor.getIconoE().setIcon(contenedor.getImagenX());
      contenedor.getMensaje().setForeground(Color.red);
  }
        for (int w=0; w<contenedor.getExp().length;w++) {
            if (contenedor.getExp()[w] != null) {
                contenedor.getMiEvaluador().parseExpression(contenedor.getExp()[w]);
                contenedor.setErrorEnExpresion(contenedor.getMiEvaluador().hasError());//hay error? 
                if(!contenedor.isErrorEnExpresion())
                {
                    contenedor.getMensaje().setText("Seleccione el plano para mover ejes.");
                    contenedor.getIconoE().setIcon(contenedor.getPalomita());
                    contenedor.getTffun().setForeground(Color.black);
                    
                    for(int i=0;i<numPuntos-1;i++)
                    {
                        valxi =contenedor.getXmin()+i*1.0/contenedor.getEscalaX();
                        valxi1=contenedor.getXmin()+(i+1)*1.0/contenedor.getEscalaX();
                        contenedor.getMiEvaluador().addVariable("x", valxi);
                        valyi=contenedor.getMiEvaluador().getValue();
                        contenedor.getMiEvaluador().addVariable("x", valxi1);
                        valyi1 =  contenedor.getMiEvaluador().getValue();
                        xi =(int)Math.round(contenedor.getEscalaX()*(valxi));
                        yi =(int)Math.round(contenedor.getEscalaY()*valyi);
                        xi1=(int)Math.round(contenedor.getEscalaX()*(valxi1));
                        yi1=(int)Math.round(contenedor.getEscalaY()*valyi1);
                        g.setPaint(contenedor.getColores()[w]);
                        //control de discontinuidades groseras y complejos
                        Complex valC = contenedor.getMiEvaluador().getComplexValue();
                        double imgx = (double)Math.abs(valC.im());
                        if(dist(valxi,valyi,valxi1,valyi1)< 1000 && imgx==0)
                        {
                            g.draw(new Line2D.Double(xg+xi,yg-yi,xg+xi1,yg-yi1));
                        }
                    }//fin del for
                    contenedor.getMensaje().setForeground(new Color(0,100,0));
                }else{
                    contenedor.getMensaje().setText("Hay un error en la sintaxis de la función.");
                    contenedor.getIconoE().setIcon(contenedor.getImagenX());
                    contenedor.getMensaje().setForeground(Color.red);
                    contenedor.getTffun().setForeground(Color.red);
                }         } //Comprobar que la expresión no esta vacia
        } //Bucle de las expresiones en el arreglo
 }//Graficar
 
  public double dist(double xA,double yA, double xB,double yB){
     return (xA - xB)*(xA - xB)+(yA - yB)*(yA - yB);
  }//
 
 } // class
