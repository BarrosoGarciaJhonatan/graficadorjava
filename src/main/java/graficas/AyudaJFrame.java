/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package graficas;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;

/**
 *
 * @author JHONATAN
 */
public class AyudaJFrame extends JFrame
{
   private Graficador frame;

   public AyudaJFrame(Graficador mFrame) 
   {
    super ("Ayuda");
    frame=mFrame;
    Container content_pane = getContentPane (); 

    //p = new JTextArea(30,40);
    //p.setText(information()); 
    //p.setEditable(false);

    //JScrollPane sp = new JScrollPane(p);
    JLabel titulo = new JLabel("FUNCIONES MATEMÁTICAS");
    titulo.setFont(new Font("Arial",Font.BOLD,16));
    titulo.setForeground(Color.black);
    titulo.setBackground(new Color(255,214,155));
    titulo.setOpaque(true);
    titulo.setHorizontalAlignment(JLabel.CENTER);
    JList lista = new JList(information().split("\n"));
    JScrollPane sp = new JScrollPane(lista);

    content_pane.add(titulo,BorderLayout.NORTH);
    content_pane.add(lista,BorderLayout.CENTER);
    pack ();
    
    ImageIcon icono = new ImageIcon(getClass().getResource("images/calculator.png")); 
    this.setIconImage(icono.getImage());
    this.setDefaultCloseOperation (JFrame.DISPOSE_ON_CLOSE);
    this.setLocationRelativeTo(null);
    this.setResizable(false);
   } 


  String information(){
    return 
    " + suma x+10\n"
    + " - resta 2x-25\n"
    + " * multiplicación 3*x\n"
    + " / división 1/x\n"
    + " () agrupación (x+4)/(3*x)\n"
    + " ^ potenciación (-3*x)^2\n"
    + " sqrt() raíz cuadrada sqrt(x)\n"
    + " sen() seno sen(x^2)\n"
    + " cos() coseno 6*cos(-3*x)\n"
    + " tan() tangente 3*tan(x)\n"
    + " atan() arcotangente atan(x-3)\n"
    + " asin() arcoseno asen((x+5)/(3^x))\n"
    + " acos() arcocoseno acos(-x+3)\n"
    + " sinh() seno hiperbólico sinh(x)\n"
    + " cosh() coseno hiperbólico -4*cosh(1/x)\n"
    + " tanh() tangente hiperbólica tanh(x)/2\n"
    + " asinh() arcoseno hiperbólico 2*asinh(x)/3\n"
    + " acosh() arcocoseno hiperbólico (2+acosh(x))/(1-x)\n"
    + " atanh() arcotangente hiperbólica atanh(x)*(3-x^(1/x))\n"
    + " ln() logaritmo natural ln(x)+1\n"
    + " log() logaritmo decimal -2*log(x)-1\n"
    + " abs() valor absoluto abs(x-2)\n"
    + " pi 3,141592653589793\n"
    + " e 2,718281828459045\n";
}//information

} // class AyudaFrame

