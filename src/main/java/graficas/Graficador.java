package graficas;

import javax.swing.*;
import javax.swing.BorderFactory; 
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.awt.image.DirectColorModel;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.plaf.metal.MetalLookAndFeel;
import org.nfunk.jep.*;  

public class Graficador extends JFrame 
{ 
 private    JEP miEvaluador;
 private boolean    errorEnExpresion; //si hay error de sintaxis en la función
 private boolean    errorEnNumero   ; //si hay error de sintaxis en la función
 public final Font FT12 = new Font("Arial",Font.PLAIN,12);
 public final Font FT14 = new Font("Arial",Font.PLAIN,14);
 public final Font FT9  = new Font("Arial",Font.PLAIN,9);
 private JTextField tffun; 
 private JLabel mensaje; 
 private JPanel sP; //Slider Panel
 private JPanel zG; //aquí se va a poner la ZonaGrafica
 private JPanel controlPanel ; //panel para botones y campos de texto,etc 
 private JPanel logoPanel;
 private JPanel displayPanel1 = new JPanel(); //aquí se va a poner ZG para obtener un buen borde
 private JPanel displayPanel2 = new JPanel(); //aquí se va a poner los Sliders y controlPanel
 private JButton btnLimpiar,btnGraficar; 
 private JFrame fFrame; //ventana de ayuda 
 private int galto,gancho; //dimesiones de la zona de graficación 
 private double xmin,xmax,imgx;
 private int    x0,y0;           //origen
 private int    escalaX,escalaY; 
 public final BasicStroke GROSOR1 = new BasicStroke(1.5f); //thickness
 public final float DASH1[] = {5.0f};
 public final BasicStroke DASHED = new BasicStroke(1.0f, 
                                                      BasicStroke.CAP_BUTT, 
                                                      BasicStroke.JOIN_MITER, 
                                                      5.0f, DASH1, 0.0f);
 private ImageIcon logocrv, ito; 
 
 private Color [] colores={Color.MAGENTA, Color.BLUE, Color.GRAY, Color.RED,Color.decode("#B7008E"), Color.PINK, Color.CYAN, Color.GREEN,Color.decode("#38197A"),Color.decode("#008789") };
 private String [] exp;
 private int cont;
 
 private JMenuBar menu;
 private JMenu opc, info,nuevo, ayuda;
 private JMenuItem catalogo, graficas,ecuacion,salir,acerca,guardar;
 private GraficasP funciones;
 private About fAbout;
 private boolean seGrafico;
 /** Para guardar la gráfica en imagen **/
 private BufferedImage onscreenImage;
 private Graphics2D g2;
 private JPanel informacion;
 private JLabel iconoE;
 private Graficador ventana;
 
public Graficador(){
    loadLookAndFeel();
    exp = new String[10];
    cont = 0;
    this.setTitle("Ingeniería en Sistemas Computacionales");
    this.setSize(800,400);
    iniciar();
    this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    this.setLocationRelativeTo(null);
    ImageIcon icono = new ImageIcon(getClass().getResource("images/calculator.png")); 
    this.setIconImage(icono.getImage());
    ventana = this;
}
 public void iniciar() 
 {       
	 Container contenedor = getContentPane(); 
         menu = new JMenuBar();
         opc = new JMenu("Opciones");
         info = new JMenu("Información");
         nuevo = new JMenu("Nuevo");
         ayuda = new JMenu("Ayuda");
         catalogo = new JMenuItem("Catálogo de funciones");
         graficas = new JMenuItem("Funciones graficadas");
         ecuacion = new JMenuItem("Ecuación explicita");
         acerca = new JMenuItem("Acerda de ...");
         salir = new JMenuItem("Salir");
         guardar = new JMenuItem("Guardar como ...");
         ayuda.add(acerca);
         opc.add(catalogo);
         //opc.add(new JSeparator());
         opc.add(guardar);
         opc.add(new JSeparator());
         opc.add(salir);
         nuevo.add(ecuacion);
         info.add(graficas);
         menu.add(opc);
         menu.add(nuevo);
         menu.add(info);
         menu.add(ayuda);
         EventoMenu emenu = new EventoMenu(this);
         catalogo.addActionListener(emenu);
         graficas.addActionListener(emenu);
         ecuacion.addActionListener(emenu);
         salir.addActionListener(emenu);
         guardar.addActionListener(emenu);
         acerca.addActionListener(emenu);
         this.addKeyListener(emenu);
         this.setJMenuBar(menu);
	 logocrv = new ImageIcon(getClass().getResource("images/borrador.png")); 
         ito =  new ImageIcon(getClass().getResource("images/logo.png")); 
         ImageIcon logo=new ImageIcon(logocrv.getImage().getScaledInstance(50, 45, Image.SCALE_SMOOTH));
	 //Gancho = getSize().width-10;
	 //Galto = 70*getSize().height/100; 
         gancho = this.getWidth()*2;
	 galto = this.getHeight()*2;
	 zG = new ZonaGrafica(this); //zona gráfica
	 sP = new SliderPanel(this); //panel para Sliders de escala	
	 controlPanel = new JPanel();
	 displayPanel1.setLayout(new BorderLayout());
	 displayPanel1.add(zG, BorderLayout.CENTER); 
	 //Logopanel
	 logoPanel = new JPanel();
         logoPanel.setLayout(new BorderLayout());
         JLabel borrar = new JLabel(logo);
         borrar.setVerticalAlignment(JLabel.CENTER);
	 logoPanel.add(borrar, BorderLayout.CENTER); 
         logoPanel.add(new JLabel("Borrar todo"), BorderLayout.SOUTH); 
         borrar.setToolTipText("Limpiar el plano");
         
	 borrar.addMouseListener (new MouseAdapter ()
	                        {   
                                        @Override
	                        	public void mouseClicked (MouseEvent e) 
	                            {                              
	                              if(e.getClickCount() >0)
	                              {  		                                 		                                 
	                                /** try{URI elURL= new URI("http://www.itoaxaca.edu.mx/");
                                           Desktop.getDesktop().browse(elURL);
                                      }catch(IOException | URISyntaxException ex){}**/
                                          setExp(new String[7]);
                                          setCont(0);
                                          getZG().repaint();
                                         
				      }							  
	                            }                        	                                                            
	                            
	                        });
                    
     Cursor manitaCursor = new Cursor(Cursor.HAND_CURSOR);
     logoPanel.setCursor(manitaCursor);  
    //fin logoPanel
    //Panel para exportar
        //Logopanel
	 JPanel exportarI = new JPanel();
         exportarI.setLayout(new BorderLayout());
         ImageIcon save =  new ImageIcon(getClass().getResource("images/disquete.png")); 
         ImageIcon logoG=new ImageIcon(save.getImage().getScaledInstance(40, 40, Image.SCALE_SMOOTH));
         JLabel guardarI = new JLabel(logoG);
         guardarI.setVerticalAlignment(JLabel.CENTER);
	 exportarI.add(guardarI, BorderLayout.CENTER); 
         exportarI.add(new JLabel("Guardar como..."), BorderLayout.SOUTH); 
         guardarI.setToolTipText("Exportar a PNG o JPG");
         
	 guardarI.addMouseListener (new MouseAdapter (){
                                        @Override
	                        	public void mouseClicked (MouseEvent e) {
                                            ventana.remove(ventana.getDisplayPanel2());
                                            ventana.getDisplayPanel1().updateUI();                     
                                            FileDialog chooser = new FileDialog(ventana, "Use una extensión .png o .jpg ", FileDialog.SAVE);
                                            chooser.setVisible(true);
                                            String filename = chooser.getFile();
                                            if (filename != null) {
                                                ventana.guardar(chooser.getDirectory() + File.separator + chooser.getFile());
                                            }	  
                                        }                        	                                                            
	                            
	                        });
                    
     exportarI.setCursor(manitaCursor);  
    //fin logoPanel
    //Fin del Panel para exportar imagen
	    
	 controlPanel.setLayout(new GridBagLayout()); // como hoja de Excel 
	 GridBagConstraints c = new GridBagConstraints();
	 c.fill = GridBagConstraints.HORIZONTAL;
	
         //espacio en blanco
	 JLabel esp1 = new JLabel("f(x) = "); 
         esp1.setFont(new Font("Arial",Font.BOLD,25));
	 c.gridy = 1; // fila
	 c.gridx = 0; // columna 
	 controlPanel.add(esp1,c); 
         
	 tffun = new JTextField("",30);
         tffun.setFont(new Font("Arial",Font.PLAIN,16));
         tffun.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.BLACK));
         tffun.setPreferredSize(new Dimension(200,25));
	 c.gridy = 1; // fila
	 c.gridx = 1; // columna 
	 controlPanel.add(tffun,c);
         tffun.setToolTipText("Ingresa la función en términos de la variable x.");
	 //espacio en blanco
	 JLabel esp2 = new JLabel(" "); 
         esp1.setFont(new Font("Arial",Font.BOLD,25));
	 c.gridy = 1; // fila
	 c.gridx = 2; // columna 
	 controlPanel.add(esp2,c); 	 
	
	 btnGraficar = new JButton("Graficar"); 
         btnGraficar.setPreferredSize(new Dimension(120,25));
         btnGraficar.setBorder(BorderFactory.createMatteBorder(3, 3, 3, 3,new Color(48,68,181)));
         btnGraficar.setBackground(new Color(175, 188, 219));
	 c.gridy = 1; // fila
	 c.gridx = 3; // columna 
	 controlPanel.add(btnGraficar,c); 
	 btnGraficar.setFont(FT14);
	
	 btnLimpiar = new JButton("Cancelar");
         btnLimpiar.setPreferredSize(new Dimension(120,25));
	 c.gridy = 3; // fila
	 c.gridx = 3; // columna 
	 controlPanel.add(btnLimpiar,c); 
	 btnLimpiar.setFont(FT14);
         btnLimpiar.setBorder(BorderFactory.createMatteBorder(3, 3, 3, 3,new Color(175, 30, 45)));
         btnLimpiar.setBackground(new Color(244,201,201));
	
	informacion = new JPanel(new FlowLayout(FlowLayout.LEFT));
        iconoE = new JLabel();
        iconoE.setPreferredSize(new Dimension(32,32));
	mensaje = new JLabel("",JLabel.LEFT); 
        informacion.add(iconoE);
        informacion.add(mensaje);
	 c.gridwidth = 3;
	 c.gridy = 4; // fila
	 c.gridx = 0; // columna 
         mensaje.setFont(new Font("Arial",Font.BOLD,15));
	 mensaje.setForeground(new Color(0,100,0));
	 controlPanel.add(informacion,c); 
	 //fin del administrador de diseño ControlPanel
	
	 //Bordes 
	 Border colorline = BorderFactory.createLineBorder(new Color(220,220,220)); 
	 displayPanel1.setBorder(colorline); 
	 TitledBorder rotulo; 
	 rotulo = BorderFactory.createTitledBorder(" Escala");
	 rotulo.setTitleFont(FT12);
	 rotulo.setTitleColor(new Color(0,0,128));
	 sP.setBorder(rotulo); 
	
	 rotulo = BorderFactory.createTitledBorder("Herramientas");
	 rotulo.setTitleFont(FT12);
	 rotulo.setTitleColor(new Color(0,0,128));
	 logoPanel.setBorder(rotulo); 
         
         rotulo = BorderFactory.createTitledBorder("Exportar");
	 rotulo.setTitleFont(FT12);
	 rotulo.setTitleColor(new Color(0,0,128));
         exportarI.setBorder(rotulo); 
	
	 rotulo = BorderFactory.createTitledBorder(" Función f(x)");
	 rotulo.setTitleColor(new Color(0,0,128));
	 rotulo.setTitleFont(FT12);
	 controlPanel.setBorder(rotulo); 
	 //fin de Bordes 
	
	 displayPanel1.setPreferredSize( new Dimension(gancho,galto)); 
	 controlPanel.setPreferredSize( new Dimension(40*gancho/100,10*getSize().height/100));
	 sP.setPreferredSize( new Dimension(30*gancho/100,30*getSize().height/100));
	
	 displayPanel2.setLayout(new BorderLayout(1,1)); 
         JPanel miPanel = new JPanel(new GridLayout(1,2));
         miPanel.add(logoPanel);
         miPanel.add(exportarI);
	 displayPanel2.add("West", miPanel);
	 displayPanel2.add("Center",controlPanel);
	 displayPanel2.add("East", sP);
	
	 contenedor.setLayout(new BorderLayout(1,1)); //aplicar al frame
	 contenedor.add("Center", displayPanel1);
	// Contenedor.add("North", displayPanel2); 
	 
	 miEvaluador = new JEP();                     
	 miEvaluador.addStandardFunctions();  //agrega las funciones matematicas comunes
	 miEvaluador.addStandardConstants();  
	 miEvaluador.addComplex();  		
	 miEvaluador.addFunction("sen", new org.nfunk.jep.function.Sine());//habilitar 'sen'			
	 miEvaluador.addVariable("x", 0); 
	 miEvaluador.setImplicitMul(true); //permite 2x en vez de 2*x
	 escalaX=30;
	 escalaY=30;
	 x0=gancho/2;
	 y0=galto/2;
	 
	 ManejadorDeEvento manejadorDevt = new  ManejadorDeEvento(this);		 	
	 tffun.addActionListener(manejadorDevt);		
	 btnGraficar.addActionListener(manejadorDevt);
         fFrame = new AyudaJFrame (this);
	 btnLimpiar.addActionListener(emenu);
	 funciones = new GraficasP(this);
         fAbout = new About(this);
}//
 



private class ManejadorDeEvento implements ActionListener
{
  private Graficador ventana;
  public ManejadorDeEvento(Graficador g){
      ventana = g;
  }
  public void actionPerformed (ActionEvent evt)
  { 
    Object source = evt.getSource ();
    // si se presiona el botón o se da 'enter' en algún campo de texto
    if ( source == btnGraficar || source == tffun)
    {
        ventana.remove(ventana.getDisplayPanel2());
        ventana.getDisplayPanel1().updateUI();

      //zG.repaint();
    }
     
       	     
  }
}//

// Método para exportar el gráfico a imagen

    public  void guardar(String filename) {
        validateNotNull(filename, "filename");
        int w=this.getZG().getWidth();
        int h=this.getZG().getHeight(); 
        onscreenImage  =new BufferedImage(this.getZG().getWidth(),this.getZG().getHeight(), BufferedImage.TYPE_INT_ARGB);
        g2= onscreenImage.createGraphics();
        this.getZG().paint(g2);
        
        g2.setColor(Color.BLUE);
        g2.setFont(new Font("Times New Roman",Font.BOLD,16));
        g2.drawString("@Jhonatan Barroso García",20,20);
       
        g2.dispose();
        File file = new File(filename);
        String suffix = filename.substring(filename.lastIndexOf('.') + 1);

        // png files
        if ("png".equalsIgnoreCase(suffix)) {
            try {
                ImageIO.write(onscreenImage, suffix, file);
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }

        // need to change from ARGB to RGB for JPEG
        // reference: http://archives.java.sun.com/cgi-bin/wa?A2=ind0404&L=java2d-interest&D=0&P=2727
        else if ("jpg".equalsIgnoreCase(suffix)) {
            WritableRaster raster = onscreenImage.getRaster();
            WritableRaster newRaster;
            newRaster = raster.createWritableChild(0, 0, w, h, 0, 0, new int[] {0, 1, 2});
            DirectColorModel cm = (DirectColorModel) onscreenImage.getColorModel();
            DirectColorModel newCM = new DirectColorModel(cm.getPixelSize(),
                                                          cm.getRedMask(),
                                                          cm.getGreenMask(),
                                                          cm.getBlueMask());
            BufferedImage rgbBuffer = new BufferedImage(newCM, newRaster, false,  null);
            try {
                ImageIO.write(rgbBuffer, suffix, file);
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }

        else {
            System.out.println("Invalid image file type: " + suffix);
        }
    }
    // throw an IllegalArgumentException if s is null
    private static void validateNotNull(Object x, String name) {
        if (x == null) throw new IllegalArgumentException(name + " is null");
    }
   

private static void loadLookAndFeel() {
    try {
        UIManager.put("Menu.background", Color.WHITE);
        UIManager.put("Menu.foreground", Color.BLACK);
        
        UIManager.put("Menu.selectionBackground", Color.decode("#3044B5"));
        UIManager.put("MenuItem.foreground", Color.BLACK);
        UIManager.put("Menu.selectionForeground", Color.WHITE);
        
        UIManager.put("MenuItem.background", Color.decode("#C4D8E2"));
        UIManager.put("MenuItem.selectionBackground", Color.decode("#60AFDD"));
        UIManager.put("MenuItem.selectionForeground", Color.BLACK);
        
        UIManager.setLookAndFeel(new MetalLookAndFeel());
    } catch (NumberFormatException | UnsupportedLookAndFeelException e) {
        e.printStackTrace();
    }
}
// Cambiar el icono del mensaje en el panel de información

    public ImageIcon getImagenX(){
      return new ImageIcon(getClass().getResource("images/botonx.png"));  
    }

    public ImageIcon getPalomita(){
      return new ImageIcon(getClass().getResource("images/comprobado.png"));  
    }

//Getters and Setters
    public JEP getMiEvaluador() {
        return miEvaluador;
    }

    public void setMiEvaluador(JEP miEvaluador) {
        this.miEvaluador = miEvaluador;
    }

    public boolean isErrorEnExpresion() {
        return errorEnExpresion;
    }

    public void setErrorEnExpresion(boolean errorEnExpresion) {
        this.errorEnExpresion = errorEnExpresion;
    }

    public boolean isErrorEnNumero() {
        return errorEnNumero;
    }

    public void setErrorEnNumero(boolean errorEnNumero) {
        this.errorEnNumero = errorEnNumero;
    }

    public JTextField getTffun() {
        return tffun;
    }

    public void setTffun(JTextField tffun) {
        this.tffun = tffun;
    }

    public JLabel getMensaje() {
        return mensaje;
    }

    public void setMensaje(JLabel Mensaje) {
        this.mensaje = Mensaje;
    }

    public JPanel getSP() {
        return sP;
    }

    public void setSP(JPanel SP) {
        this.sP = SP;
    }

    public JPanel getZG() {
        return zG;
    }

    public void setZG(JPanel ZG) {
        this.zG = ZG;
    }

    public JPanel getControlPanel() {
        return controlPanel;
    }

    public void setControlPanel(JPanel controlPanel) {
        this.controlPanel = controlPanel;
    }

    public JPanel getLogoPanel() {
        return logoPanel;
    }

    public void setLogoPanel(JPanel logoPanel) {
        this.logoPanel = logoPanel;
    }

    public JPanel getDisplayPanel1() {
        return displayPanel1;
    }

    public void setDisplayPanel1(JPanel displayPanel1) {
        this.displayPanel1 = displayPanel1;
    }

    public JPanel getDisplayPanel2() {
        return displayPanel2;
    }

    public void setDisplayPanel2(JPanel displayPanel2) {
        this.displayPanel2 = displayPanel2;
    }

    public int getGalto() {
        return galto;
    }

    public void setGalto(int galto) {
        this.galto = galto;
    }

    public int getGancho() {
        return gancho;
    }

    public void setGancho(int gancho) {
        this.gancho = gancho;
    }

    public double getXmin() {
        return xmin;
    }

    public void setXmin(double xmin) {
        this.xmin = xmin;
    }

    public double getXmax() {
        return xmax;
    }

    public void setXmax(double xmax) {
        this.xmax = xmax;
    }

    public double getImgx() {
        return imgx;
    }

    public void setImgx(double imgx) {
        this.imgx = imgx;
    }

    public int getX0() {
        return x0;
    }

    public void setX0(int x0) {
        this.x0 = x0;
    }

    public int getY0() {
        return y0;
    }

    public void setY0(int y0) {
        this.y0 = y0;
    }

    public int getEscalaX() {
        return escalaX;
    }

    public void setEscalaX(int escalaX) {
        this.escalaX = escalaX;
    }

    public int getEscalaY() {
        return escalaY;
    }

    public void setEscalaY(int escalaY) {
        this.escalaY = escalaY;
    }

    public ImageIcon getLogocrv() {
        return logocrv;
    }

    public void setLogocrv(ImageIcon logocrv) {
        this.logocrv = logocrv;
    }

    public Color[] getColores() {
        return colores;
    }

    public void setColores(Color[] colores) {
        this.colores = colores;
    }

    public String[] getExp() {
        return exp;
    }

    public void setExp(String[] exp) {
        this.exp = exp;
    }

    public int getCont() {
        return cont;
    }

    public void setCont(int cont) {
        this.cont = cont;
    }

    public JMenuItem getGraficas() {
        return graficas;
    }

    public void setGraficas(JMenuItem graficas) {
        this.graficas = graficas;
    }

    public JPanel getsP() {
        return sP;
    }

    public void setsP(JPanel sP) {
        this.sP = sP;
    }

    public JPanel getzG() {
        return zG;
    }

    public void setzG(JPanel zG) {
        this.zG = zG;
    }

    public JButton getBtnLimpiar() {
        return btnLimpiar;
    }

    public void setBtnAyuda(JButton btnLimpiar) {
        this.btnLimpiar = btnLimpiar;
    }

    public JButton getBtnGraficar() {
        return btnGraficar;
    }

    public void setBtnGraficar(JButton btnGraficar) {
        this.btnGraficar = btnGraficar;
    }

    public JMenuBar getMenu() {
        return menu;
    }

    public void setMenu(JMenuBar menu) {
        this.menu = menu;
    }

    public JMenu getOpc() {
        return opc;
    }

    public void setOpc(JMenu opc) {
        this.opc = opc;
    }

    public JMenu getInfo() {
        return info;
    }

    public void setInfo(JMenu info) {
        this.info = info;
    }

    public JMenuItem getCatalogo() {
        return catalogo;
    }

    public void setCatalogo(JMenuItem c) {
        this.catalogo = c;
    }

    public GraficasP getFunciones() {
        return funciones;
    }

    public void setFunciones(GraficasP funciones) {
        this.funciones = funciones;
    }

    public JFrame getfFrame() {
        return fFrame;
    }

    public JMenu getNuevo() {
        return nuevo;
    }

    public void setNuevo(JMenu nuevo) {
        this.nuevo = nuevo;
    }

    public JMenuItem getEcuacion() {
        return ecuacion;
    }

    public void setEcuacion(JMenuItem ecuacion) {
        this.ecuacion = ecuacion;
    }

    public JMenuItem getSalir() {
        return salir;
    }

    public void setSalir(JMenuItem salir) {
        this.salir = salir;
    }

    public boolean isSeGrafico() {
        return seGrafico;
    }

    public void setSeGrafico(boolean seGrafico) {
        this.seGrafico = seGrafico;
    }

    public JMenuItem getAcerca() {
        return acerca;
    }

    public void setAcerca(JMenuItem acerca) {
        this.acerca = acerca;
    }

    public JMenuItem getGuardar() {
        return guardar;
    }

    public void setGuardar(JMenuItem guardar) {
        this.guardar = guardar;
    }

    public About getfAbout() {
        return fAbout;
    }

    public void setfAbout(About fAbout) {
        this.fAbout = fAbout;
    }

    public ImageIcon getIto() {
        return ito;
    }

    public void setIto(ImageIcon ito) {
        this.ito = ito;
    }

    public JLabel getIconoE() {
        return iconoE;
    }

    public void setIconoE(JLabel iconoE) {
        this.iconoE = iconoE;
    }
    
    

} // end 




