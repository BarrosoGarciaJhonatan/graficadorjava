/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package graficas;

import java.awt.GridLayout;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author JHONATAN
 */
public class SliderPanel extends JPanel {
   private JSlider xSlider,ySlider; // Manejo de escala   	
   private Graficador contenedor;
   
   public SliderPanel(Graficador contenedor){ 
       
        this.contenedor = contenedor;
        setLayout(new GridLayout(2,1));
        SliderListener auditor = new SliderListener();
         
        xSlider = new JSlider(JSlider.HORIZONTAL, 0, 100, 20){
            @Override
                public void updateUI() {
                    setUI(new CustomSliderUI(this));
                }
        };
        //xSlider.setMajorTickSpacing(20);
   	xSlider.addChangeListener(auditor); 
        xSlider.setToolTipText("Escala del Eje X");
   	this.add(xSlider);   
   	 	  
   	ySlider = new JSlider(JSlider.HORIZONTAL, 0, 100, 20){
             @Override
                public void updateUI() {
                    setUI(new CustomSliderUI(this));
                }
        };
        //ySlider.setMajorTickSpacing(20);
   	ySlider.addChangeListener(auditor); 
        ySlider.setToolTipText("Escala del Eje Y");
   	this.add(ySlider);
   	 	    	 	
   	//xSlider.setLabelTable(xSlider.createStandardLabels(20));                  
        xSlider.setMajorTickSpacing(20);
        xSlider.setMinorTickSpacing(20);
        xSlider.setPaintTicks(true);
        xSlider.setPaintLabels(true);
        xSlider.setPaintTrack(true);
         
        ySlider.setMajorTickSpacing(20);
        ySlider.setMinorTickSpacing(20);
        ySlider.setPaintTicks(true);
        ySlider.setPaintLabels(true);   
        ySlider.setPaintTrack(true);
                         	   	 	   	 	
   	}
   	 
    public void ajusteEscala(){ // se ejecuta si se presenta algún cambio en algún Slider
        contenedor.setEscalaX((int) xSlider.getValue());    	                    
        contenedor.setEscalaY((int) ySlider.getValue());			            
        contenedor.getZG().repaint();
    }//
   	 
   	 
private class SliderListener implements ChangeListener 
   	 {
                @Override
	        public void stateChanged(ChangeEvent e) 
	        {  		        		         
		          //JSlider source = (JSlider)e.getSource();
		         // if (!source.getValueIsAdjusting()) 
		          
		            ajusteEscala();
		                 	    
	        }    
     }          	    	    	 
  } //            	