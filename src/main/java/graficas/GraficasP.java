package graficas;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author JHONATAN
 */
public class GraficasP extends JFrame implements ActionListener{
    private String enc="f(x)=";
    private JPanel contenedor;
    private JButton actualizar;
    private Graficador ventana;
    
    public GraficasP(Graficador ventana){
       super("f(x)");
       this.ventana = ventana;
       JMenuBar menu = new JMenuBar();
       JMenu opc = new JMenu("Opciones");
       JMenuItem act = new JMenuItem("Actualizar");
       menu.add(opc);
       opc.add(act);
       act.addActionListener(this);
       this.setJMenuBar(menu);
       ImageIcon icono = new ImageIcon(getClass().getResource("images/calculator.png")); 
       contenedor = new JPanel();
       llenar();
       actualizar = new JButton("Actualizar");
       this.add(contenedor);
       pack();
       this.setIconImage(icono.getImage());
       this.setDefaultCloseOperation (JFrame.DISPOSE_ON_CLOSE);
       this.setLocationRelativeTo(null);
       this.setResizable(false);
    }
    public void llenar(){
        contenedor.removeAll();
        contenedor.setLayout(new GridLayout(11,1));
        contenedor.setBackground(Color.white);
        JLabel titulo = new JLabel("Lista de funciones");
        titulo.setForeground(Color.BLACK);
        titulo.setFont(new Font("Arial",Font.PLAIN,25));
        //titulo.setBackground(new Color(255,214,155));
        //titulo.setOpaque(true);
        titulo.setHorizontalAlignment(JLabel.CENTER);
        contenedor.add(titulo);
        for(int i=0; i<ventana.getExp().length; i++){
            if(ventana.getExp()[i]!= null){
            JLabel etiqueta = new JLabel();
            JLabel cEtiqueta = new JLabel();
            cEtiqueta.setOpaque(true);
            cEtiqueta.setPreferredSize(new Dimension(30,30));
            etiqueta.setFont(new Font("Arial",Font.BOLD,20));
            cEtiqueta.setBackground(ventana.getColores()[i]);
            etiqueta.setText(enc+ventana.getExp()[i]);
            JPanel pL = new JPanel();
            pL.setLayout(new FlowLayout(FlowLayout.LEFT));
            pL.setBackground(Color.WHITE);
            pL.add(cEtiqueta);
            pL.add(etiqueta);
            //etiqueta.setOpaque(true);
            //etiqueta.setBackground(Color.white);
            contenedor.add(pL);
            }
        }
        contenedor.updateUI();
    }

        @Override
        public void actionPerformed(ActionEvent arg0) {
            /*this.dispose();
            GraficasP historial =new GraficasP();
            historial.setVisible(true);*/
            llenar();
        }

    public String getEnc() {
        return enc;
    }

    public void setEnc(String enc) {
        this.enc = enc;
    }

    public JPanel getContenedor() {
        return contenedor;
    }

    public void setContenedor(JPanel contenedor) {
        this.contenedor = contenedor;
    }

    public JButton getActualizar() {
        return actualizar;
    }

    public void setActualizar(JButton actualizar) {
        this.actualizar = actualizar;
    }

    public Graficador getVentana() {
        return ventana;
    }

    public void setVentana(Graficador ventana) {
        this.ventana = ventana;
    }
        
        
    }
